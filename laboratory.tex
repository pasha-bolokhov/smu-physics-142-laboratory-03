%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \huge \bf \color{red} Laboratory III  }\\[2mm]
{  \Large \textbf{\textit{\color{red} Electrostatics}}  }\\[2mm]
{  \large \it \color{azure(colorwheel)} Introductory Physics II --- Physics 142/172L  }
\end{center}
\hrule
\bigskip
\begin{tabularx}{\textwidth}{lXr}
%
	&
	{\bf Name:}
	&
	{\bf Laboratory Section:}
	\hspace{1.0cm}
	\\[2mm]
%
	&
	{\bf Partners:}\hspace{5.0cm}
\end{tabularx}
\medskip


%\def\SHOWSOLUTIONS{1}
%\newcommand\solution[1]{{\ifdefined\SHOWSOLUTIONS\color{richelectricblue}#1\fi}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            I N T R O D U C T I O N                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Introduction}}

	In this laboratory we will investigate the interaction between objects
	which carry next electrical charges

\bigskip\noindent
	Last semester we were primarily concerned with the gravitational force and its interactions with matter.
	The property of matter which controlled this interaction was \emph{mass}.
	In this laboratory, you will be primarily concerned with the electromagnetic force,
	and its manifestations in terms of electricity.
	The property of matter which controls these interactions is \emph{charge}.
	The symbol for charge is traditionally \cc{ q } or \cc{ Q }.
	The SI unit of charge is the Coulomb, abbreviated as \cc{ \si{\coulomb} }

\bigskip\noindent
	A big difference between charge and mass is that while mass is always positive (or zero),
	charge can be either positive or negative (or zero).
	The smallest amount of charge ever found on a free particle is that on a single electron or proton.
	The proton and electron have opposite charges which are \emph{precisely} equal in magnitude,
	at least as well as experimental measurements have been able to determine.
	The charge on the electron is negative and the charge on a proton is positive ---
	both have a magnitude of \cc{ \SI{1.603e-19}{\coulomb} }

\bigskip\noindent
	Most objects around you consist of equal numbers of protons and electrons,
	such that the sum of all charges is zero.
	Such objects are \emph{neutral}.
	For example, an aluminum sphere of volume \cc{ \SI{1}{\cubic\centi\metre} }
	consists of \cc{ \SI{6d22}{} } aluminum atoms,
	each of which has \cc{ 13 } positively charged protons, \cc{ 13 } negatively charged electrons
	and \cc{ 14 } uncharged neutrons.
	The total positive charge of the protons in this sphere is thus
	\cc{ (\SI{13}{protons\per atom}) } \cc{ \times }
	\cc{ (\SI{6d22}{atoms}) } \cc{ \times }
	\cc{ (\SI{16d-19}{\coulomb\per proton}) } \cc{ = }
	\cc{ \SI{1.3d5}{\coulomb} },
	which is equal and opposite to the total negative charge.
	Objects can acquire a net charge by the addition or subtraction of small amounts of positive
	or negative charges.
	The interactions you observe in this laboratory are due to transferring less than a part per billion
	of the total positive or negative charge on the object

\bigskip\noindent
	Neutral objects can also become polarized, with an excess of positive charge on one side of the object,
	and of negative charge on the other.
	Again, effects can be seen with only a very small displacement of charge

\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           C O U L O M B   F O R C E                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textit{Coulomb Force}}

	The magnitude of the Coulomb, or electrostatic, force between two charged objects depends on the charge
	of the objects and the distance between them as,
\ccc
\[
	F_\text{E}	~~=~~	\frac{1}{4\pi\epsilon_0}\,\frac{Q_1\,Q_2}{R^2}
	\ccb\,,
	\tag*{(Coulomb's law)}
\]
\ccb
	where \cc{ Q_1 } and \cc{ Q_2 } are the (signed) magnitudes of the two charges,
	\cc{ R } is the distance between them, and the constant
\ccc
\[
	\frac{1}{4\pi\epsilon_0}	~~=~~	\SI{8.99d9}{\newton\cdot\square\metre\per\,\square\coulomb}
	\ccb\,,
\]
\ccb
	is a fundamental quantity called the \emph{Coulomb's constant}

\bigskip\noindent
	As much as we would like to make you derive this relationship,
	the equipment available is not sophisticated enough (or the weather dry enough)
	for you to be able to derive this relationship in a three hour period,
	so you will have to take our word for it.
	You should compare the above formula with the gravitational force law,
\ccc
\[
	F_\text{G}	~~=~~	G\,\frac{M_1\,M_2}{R^2}
	\ccb\,,
	\tag*{(Gravitational law)}
\]
\ccb
	where \cc{ M_1 } and \cc{ M_2 } are the masses of two gravitating objects,
	\cc{ R } the distance between them,
	and the gravitational constant \cc{ G ~=~ \SI{6}{\newton\cdot\square\metre\per\,\square\kg} }.
	The existence of both positive and negative charges mean that the Coulomb force
	can be either attractive or repulsive (attractive for charges of opposite sign,
	repulsive for charges of the same sign),
	whereas the gravitational force is always attractive.
	Both forces act in a direction along the line connecting the two objects.
	The orders of magnitude difference in the force constants \cc{ k } and \cc{ G }
	means that very small charges can exert very large forces

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    E L E C T R I C A L   M A T E R I A L S                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textit{Electrical Materials}}

\begin{wrapfigure}[11]{r}{0.5\textwidth}
\vskip -0.4cm
\begin{tikzpicture}
	% neutral object
	\begin{scope}
		% object
		\tikzmath{
			\len = 6.0;
			\wid = 1.6;
			\halflen = \len / 2.0;
			\halfwid = \wid / 2.0;
		}
		\draw [draw = none, top color = shadow!40, bottom color = shadow, shading angle = -40,
			rounded corners]
			(-\halflen, -\halfwid)	rectangle	++(\len, \wid);
		\node at (0, -1.2)
			{\color{ProcessBlue} neutral, unpolarized};

		% charges
		\tikzmath{
			\ncharges = 10;
			\step = \len / \ncharges;
			\rad = \step / 2.0 - 0.1;
			\yy = \halfwid - \step / 2.0;
			\numrows = round(\wid / \step);
			\ystep = \wid / \numrows;
		}
		\foreach \j in {1, ..., \numrows} {
			\tikzmath {
				\yy = -\halfwid + \j * \ystep - \ystep / 2.0;
			}
			\foreach \k in {1, ..., \ncharges} {
				\tikzmath {
					\xx = -\halflen + \step * \k - \step / 2.0;
					\idx = \j + \k;
					if (round(\idx / 2) * 2 == \idx) then {
						let \a = {ruddy};
						let \chargesym = {+};
					} else {
						let \a = {oceanboatblue};
						let \chargesym = {-};
					};
				}
				\draw [draw = none, ball color = \a, fill opacity = 0.7]
					(\xx, \yy)	circle	[radius = \rad]
							node	{\color{peridot} $\mathbold\chargesym$};
			}
		}
	\end{scope}

	% charged object
	\begin{scope}[shift = {(0, -2.8)}]
		% object
		\tikzmath{
			\len = 6.0;
			\wid = 1.6;
			\halflen = \len / 2.0;
			\halfwid = \wid / 2.0;
		}
		\draw [draw = none, top color = shadow!40, bottom color = shadow, shading angle = -40,
			rounded corners]
			(-\halflen, -\halfwid)	rectangle	++(\len, \wid);
		\node at (-2.2, -1.4)
			{\begin{minipage}[t]{1.4cm}\raggedright\color{ProcessBlue}positive end\end{minipage}};
		\node at (2.2, -1.4)
			{\begin{minipage}[t]{1.4cm}\raggedleft\color{ProcessBlue}negative end\end{minipage}};

		% charges
		\tikzmath{
			\ncharges = 10;
			\step = \len / \ncharges;
			\rad = \step / 2.0 - 0.1;
			\yy = \halfwid - \step / 2.0;
			\numrows = round(\wid / \step);
			\ystep = \wid / \numrows;
		}
		\foreach \j in {1, ..., \numrows} {
			\tikzmath {
				\yy = -\halfwid + \j * \ystep - \ystep / 2.0;
			}
			\foreach \k in {1, ..., \ncharges} {
				\tikzmath {
					\xx = -\halflen + \step * \k - \step / 2.0;
					\idx = \k + 1;
					if (round(\idx / 2) * 2 == \idx) then {
						let \a = {ruddy};
						let \chargesym = {+};
					} else {
						let \a = {oceanboatblue};
						let \chargesym = {-};
					};
				}
				\draw [draw = none, ball color = \a, fill opacity = 0.7]
					(\xx, \yy)	circle	[radius = \rad]
							node	{\color{peridot} $\mathbold\chargesym$};
			}
		}
	\end{scope}

	% charged sphere
	\begin{scope}[shift = {(5, -1.4)}]
		\tikzmath {
			\rad = 0.8;
			\smallrad = 0.2;
		}
		% object
		\draw [draw = none, ball color = celadon, opacity = 0.8]
			(0, 0)	circle [radius = \rad];

		% little charges
\newcommand{\LittlePositiveCharge}[2]{
		\draw [draw = none, ball color = ruddy, opacity = 0.7]
			(#1, #2)	circle [radius = \smallrad]
					node	{\color{peridot} $\mathbold+$};
}
		\LittlePositiveCharge{0}{0}
		\LittlePositiveCharge{-0.5}{0}
		\LittlePositiveCharge{0.5}{0}
		\LittlePositiveCharge{0.25}{0.4}
		\LittlePositiveCharge{-0.25}{0.4}
		\LittlePositiveCharge{0.25}{-0.4}
		\LittlePositiveCharge{-0.25}{-0.4}
		\node at (0, -1.2)	{\color{ProcessBlue} charged object};
	\end{scope}
\end{tikzpicture}
\end{wrapfigure}

	Materials can be categorized in terms of how easily the charges (usually electrons, sometimes ions)
	can move within them.
	In a \emph{conductor} (\emph{e.g.} metals), a few electrons per atom are ``free'' to move within the solid.
	If a positively charged object is brought near one end of a conductor,
	electrons will be attracted towards that point, resulting in a net negative charge near that point,
	and a net positive charge on the rest of the conductor.
	That net positive charges is due to the excess of positive protons (in nuclei) that are left behind.
	If a conductor becomes charged, then the free electrons will distribute themselves uniformly all around
	the outside of the conductor.
	Qualitatively, in a negatively charged conductor, the excess electrons distribute themselves
	to be as far apart as possible

\bigskip\noindent
	In an \emph{insulator} (glass, ceramics, rubber, most plastics), all of the electrons are firmly attached
	to individual atoms or molecules, and they are not free to move within the solid.
	You will find in this laboratory that friction can transfer charges between insulators,
	leaving one locally charged positive, and the other locally charged negative.
	The relative tendency of insulators to become positively or negatively charged
	is referred to as the triboelectric series.
	Also, if a charged insulator is brought near enough to a conductor,
	some charge can be transferred either as a spark, or directly when the two objects touch

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       M O N I T O R I N G   T H E   P R E S E N C E   O F   C H A R G E      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textit{Monitoring the Presence of Charge}}

\begin{wrapfigure}[6]{l}{0.16\textwidth}
\vskip -6mm
\begin{tikzpicture}
	% case
	\draw [line width = 0.8mm, draw = carnelian]
		(97: 1.2)	arc	[start angle = 97, end angle = 360 + 83, radius = 1.2cm];
	\draw [line width = 0.3mm, draw = blush]
		(97: 1.15)	arc	[start angle = 97, end angle = 360 + 83, radius = 1.15cm];


	% base
	\begin{scope}[shift = {(0, -1.24)}]
		\draw [draw = none, left color = carolinablue, right color = carolinablue, middle color = carolinablue!30]
			(-0.14, 0)	rectangle	(0.14, -0.4);
		\draw [draw = none, left color = carolinablue, right color = carolinablue, middle color = carolinablue!30]
			(-0.6, -0.4)	rectangle	(0.6, -0.5);
        \end{scope}

	% conductive stem
	\begin{scope}[rotate = -40]					% leaves
		\draw [draw = none, left color = Gold, right color = Gold, middle color = Gold!20]
			(-0.06, 0)	rectangle	(0.7, 0.1);
        \end{scope}
	\begin{scope}[shift = {(-0.04, 0.07)}, rotate = 180 + 40]	% leaves
		\draw [draw = none, left color = Gold, right color = Gold, middle color = Gold!10]
			(-0.06, 0)	rectangle	(0.7, 0.1);
        \end{scope}
	\draw [draw = none, left color = darkgray, right color = darkgray, middle color = darkgray!20]
		(-0.1, 0.03)	rectangle	(0.1, 1.5);	% stem
	\draw [draw = none, ball color = lightgray!40]		% terminal
		(0, 1.6)	circle	[radius = 0.24];

	% indicate charges
	\newcommand{\LittleCharge}[2]{
		\node at (#1, #2)
			{\color{amaranth}$\scriptscriptstyle\mathbold+$};
	}
	\begin{scope}[rotate = -40]
		\LittleCharge{0.1}{0.2}
		\LittleCharge{0.3}{0.2}
		\LittleCharge{0.5}{0.2}
		\LittleCharge{0.7}{0.2}
		\LittleCharge{0.86}{0.08}
		\LittleCharge{0.7}{-0.1}
		\LittleCharge{0.5}{-0.1}
		\LittleCharge{0.3}{-0.1}
		\LittleCharge{0.1}{-0.1}
	\end{scope}
	\begin{scope}[rotate = 180 + 40]
		\LittleCharge{0.3}{0.1}
		\LittleCharge{0.5}{0.1}
		\LittleCharge{0.7}{0.1}
		\LittleCharge{0.86}{-0.02}
		\LittleCharge{0.7}{-0.2}
		\LittleCharge{0.5}{-0.2}
		\LittleCharge{0.3}{-0.2}
		\LittleCharge{0.1}{-0.2}
	\end{scope}
\end{tikzpicture}
\end{wrapfigure}
	To monitor the presence of charge you will use an
	\emph{electroscope}.
	This simple device uses the deflection of a light metal foil as an indicator of the presence of charge (see figure).
	An excess of charge on the centre conductor plate is shared between the conductor and the foil,
	and the two repel each other.
	The movable foil thus moves away from the centre conductor,
	and the angle between the foil and the plate provides a measure of the excess charge

\begin{itemize}
\item[{\color{alizarin}\ding{234}}]
	Can you tell the sign of the charge just by looking at the electroscope?
	Why or why not?
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                               P R O C E D U R E                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Procedure}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           R O D S   &   C L O T H S                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\thesubsection}{\Alph{subsection}.}
\subsection{\textit{Rods \& Cloths}}

	Get a collection of rods and cloths from the supply table.
	These are insulators, and can become charged by vigorous rubbing

\bigskip\noindent
	\emph{Hint}:
	\textsl{washing the rods in soapy water and drying them, from time to time,
	should re--store their ability to hold charge}

\begin{itemize}
\item
	First rub the hard rubber rod with rabbit fur.
	\emph{Do you hear sparks? What is happening?}
	Observe the interaction between the charged fur and the rod, and of the fur itself.

	\begin{itemize}
	\item[\color{alizarin}\ding{234}]
		\emph{Explain} what you see in terms of the attractive
		and repulsive Coulomb interaction.
		The rod will be negatively charged.
		What is the charge on the fur?
		Could you tell just by looking at the fur itself, or do you need the rod?
	\end{itemize}

\item
	Rub the rod with the fur, and bring the rod slowly towards the electroscope ball.
	\emph{Does the electroscope respond? How?}
	Listen carefully for whether or not a spark jumps between the rod and the electroscope.
	Bring the rod away from the electroscope before it touches or sparks
	(if it sparks unintentionally, pull the rod away and touch the electroscope with your finger.
	This is called \emph{grounding} the electroscope.
	Then bring the rod in again without sparking).
	What happens to the electroscope?

	\begin{itemize}
	\item[\color{alizarin}\ding{234}]
		\emph{Explain} what is happening. Can you get a deflection of the electroscope foil
		when the \emph{net} charge on the electroscope is zero?
		Why or why not?
        \end{itemize}

\item
	Bring the charged rod in until it sparks or touches.
	\emph{What happens? Why?}
	While the charged rod is far away and the foil is deflected,
	touch the electroscope with your finger to \emph{ground} the electroscope.
	\emph{What happens?}

	\begin{itemize}
	\item[\color{alizarin}\ding{234}]
		\emph{Explain} the answers to these questions
	\end{itemize}
\end{itemize}

\noindent
	\emph{Draw a sketch} of the charge distribution on the electroscope for the cases of
\begin{enumerate}[label=(\alph*)]
\item
	hard rubber rod, near, but not touching the electroscope, and

\item
	after touching or having a spark jumps between the rod and the electroscope.
\end{enumerate}
	What is the net charge on the electroscope in each case?


\begin{itemize}
\item
	Charge the electroscope by touching the top plate/ball with the charged rod.
	Remove the rod, and bring it back again, this time not touching the electroscope.
	\emph{What happens? Why?}
	Now, bring the rod close to the main body of the electroscope (near the foil).
	\emph{What happens and why?}

	\begin{itemize}
	\item[\color{alizarin}\ding{234}]
		\emph{Explain} these
	\end{itemize}

\item
	Using a single pith ball (lightweight ball with a conducting surface)
	suspended from a support, observe the behaviour of a neutral (uncharged),
	free--swinging pith ball as a charged rod is brought near it.
	Bring the rod slowly closer to the pith ball until they touch

	\begin{itemize}
	\item[\color{alizarin}\ding{234}]
		\emph{Explain} the behaviour of the pith ball in terms of electric charge.
		\emph{Sketch} the charge distribution on the rod and ball before and after they touch
	\end{itemize}
\end{itemize}

\noindent
	Next, take other combinations of rods and cloths
\begin{itemize}
\item
	Rub the lucite rod with rabbit fur and repeat the above experiments
	(\emph{ground the electroscope before you start})
	\begin{itemize}
	\item[\color{alizarin}\ding{234}]
		For results which look the same as with your rubber rod,
		you only need to state that fact in your laboratory write--up.
		If there are differences, describe them.
		Can you tell from these experiments whether or not the lucite and rubber rods
		have the same charge?
	\end{itemize}

\item
	Charge your electroscope with the hard rubber rod after it has been rubbed
	with rabbit fur to give it a negative charge.
	Rub different combinations of rods and cloths together.
	Bring the charged rod close to the electroscope and observe the direction
	of movement of the electroscope
	\begin{itemize}
	\item[\color{alizarin}\ding{234}]
		Make a table of your results and use that table to construct a triboelectric series
		of your rods and cloths, from most positive to most negative
	\end{itemize}

\item
	Can you tell from these experiments whether there are exactly two kinds of
	charge (zero, or \emph{no} charge is not a kind of charge)?
	\begin{itemize}
	\item[\color{alizarin}\ding{234}]
		If so, how?
		If not, how many kinds of charged do you find?
	\end{itemize}

\end{itemize}


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   C H A R G I N G   B Y   I N D U C T I O N                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\textit{Charging by Induction}}

	In this experiment, you will investigate the phenomenon of
	\emph{charging by induction}.
	This takes advantage of the polarization of charges within conductors
	which you observed in the previous experiment

\begin{itemize}
\item
	Charge a rod with a fur.
	Bring the rod close to the electroscope,
	but do not touch it or allow a spark to jump to it.
	When the foil is deflected, and the rod is nearby,
	touch the electroscope ball with your finger.
	Remove \emph{first} your finger, and \emph{then} the charged rod.
	The foil should now move, indicating an excess of charge.
	Use your knowledge of induced polarization and the idea that your finger is a conductor
	to \emph{predict} the sign of the charge.
	Determine the sing of the charge on the electroscope
	(\emph{describe how you did this}).
	\emph{Does your measurement agree with your prediction?}
	Discuss this with your laboratory partners (and the instructor)
	until you understand why you got the result you did.
	\begin{itemize}
	\item[\color{alizarin}\ding{234}]
		Write this explanation (include a sketch of the charge distribution)
		in you laboratory report
	\end{itemize}

\item
	A similar experiment can be performed using the pith ball on an insulated stand.
	Bring a charged rod near the sphere, then touch the sphere with your finger.
	Remove your finger, and then the rod.
	\begin{itemize}
	\item[\color{alizarin}\ding{234}]
		What is the sign of the charge on the sphere?
		Does it matter where you touch the sphere
		(\emph{i.e.} near or far from the rod)?
		Why or why not?
	\end{itemize}
\end{itemize}


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Q U A N T I T A T I V E   E X P E R I M E N T                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\textit{Quantitative Experiment (optional, weather--dependent)}}

\begin{wrapfigure}[6]{r}{0.18\textwidth}
\vskip -0.4cm
\begin{tikzpicture}
	% charges
	\draw [draw = none, ball color = darkpastelblue]
		(-0.8, 0)	circle [radius = 0.3]
		node [shift = {(-0.5, 0.4)}]	{\cc{ Q_1 }};
	\draw [draw = none, ball color = darkpastelblue]
		(+0.8, 0)	circle [radius = 0.3]
		node [shift = {(0.5, 0.4)}]	{\cc{ Q_2 }};

	% strings
	\draw [ultra thick, draw = bazaar, shorten <= 3mm]
		(-0.8, 0)	--	(0, 3.0);
	\draw [ultra thick, draw = bazaar, shorten <= 3mm]
		(+0.8, 0)	--	(0, 3.0)
		node	[midway, right, xshift = 1mm]	{\cc{ l }};
	\draw [thin, draw = ProcessBlue, dashed]	% reference line
		(0, 3.0)	--	(0, 0.6);
	\draw [thin, draw = ProcessBlue, shift = {(0, 3)}]
		(-90: 2.0)	arc	[start angle = -90, end angle = -76, radius = 2.0]
		node	[midway, below]	{\cc{ \theta }};
	\draw [thin, draw = ProcessBlue, shift = {(0, 3)}]
		(-90: 1.8)	arc	[start angle = -90, end angle = -104, radius = 1.8]
		node	[midway, below]	{\cc{ \theta }};

	% label
	\draw [thin, draw = ProcessBlue, <->, > = {Stealth[length = 1.8mm]}]
		(-0.8, -0.5)	--	(0.8, -0.5)
		node [midway, above] {\cc{ r }};
	\draw [thin, draw = ProcessBlue]
		(-0.8, -0.3)	--	(-0.8, -0.6)
		(+0.8, -0.3)	--	(+0.8, -0.6);

\end{tikzpicture}
\end{wrapfigure}

	In the qualitative experiments, you saw that two objects charged with the same sign
	repelled each other.
	In this part of the laboratory you will measure the strength of this force
	relative to the force of gravity on the same object.
	Assuming Coulomb's law, you will calculate the charge on these objects

\bigskip\noindent
	You will need to perform these measurements very quickly,
	especially if it is particularly humid

\bigskip\noindent
	On the laboratory bench, you should find two pith balls (light weight, conductive surface)
	hanging on insulating strings

\begin{enumerate}[label=(\arabic*)]
\item
	Calculate the force of gravity (in \cc{ \si{\newton} }) acting on each pith ball.
	Adjust the tops of the strings so that the balls are at the same height above the table
	and are just touching when they are uncharged.
	Measure the length \cc{ l } of the strings

\item
	Charge up the two pith balls as much as you can using your choice of rods.
	Be careful not to ground the pith balls once they are charged by touching them directly
	with your hands

\item
\label{equalization}
	Touching just the strings, force the pith balls to touch each other to try and
	get the charge on the two balls to be equal.
	Release the balls

\item
	The two balls should now look like the upper figure on the right.
	Using a plastic ruler or caliper, being careful not to touch them,
	measure the separation \cc{ r } between the two balls
	(\emph{note:} \cc{ r } is the distance between the \emph{centers} of the balls).
	If the separation is less than about \cc{ 1 } diameter of the balls, try to put more
	charge on.
	If your pith balls cannot hold enough charge to separate more than \cc{ \SI{0.5}{\centi\metre} }
	no matter how hard you try, share with another laboratory group that has better luck

\item
\begin{minipage}[t]{15.8cm}
	Draw a free body diagram for one of the charged pith balls as at the right,
	where \cc{ F } is the electrostatic (Coulomb) force,
	\cc{ G ~=~ mg } is the gravitational force,
	and \cc{ T } is string tension.
	Show that if the Coulomb force is horizontal,
	then the ratio of the Coulomb force to the gravitational force on one of the balls is given by
\end{minipage}
\begin{minipage}[t]{3cm}
\vskip -1.2cm
\begin{tikzpicture}
	% charge
	\draw [draw = none, ball color = ruddy]
		(0, 0)	circle [radius = 0.3];

	% forces
	\draw [ultra thick, line cap = round, draw = blue-violet, line width = 1mm, ->, > = {Stealth[length = 6mm, width = 4mm]},
		shorten <= 4mm, shorten >= 1cm]
		(0, 0)	--	(0.8, 3)
		node [midway, left, shift = {(-2mm, -2mm)}]	{\cc{ T }};
	\draw [ultra thick, line cap = round, draw = blue-violet, line width = 1mm, ->, > = {Stealth[length = 5mm, width = 3mm]},
		shorten <= 4mm]
		(0, 0)	--	(-1.2, 0)
		node [left, below, shift = {(3mm, -2mm)}]	{\cc{ F }};
	\draw [ultra thick, line cap = round, draw = blue-violet, line width = 1mm, ->, > = {Stealth[length = 6mm, width = 4mm]},
		shorten <= 4mm]
		(0, 0)	--	(0, -1.8)
		node [midway, right, shift = {(2mm, -1mm)}]	{\cc{ G }};
\end{tikzpicture}
\end{minipage}
\vskip -1.8cm
\ccc
\[
	\frac { F }{ mg }	~~=~~	\tan \theta
	\ccb\,.
\]
\ccb
	If \cc{ \theta } is small, then
\ccc
\[
	\tan \theta	~~\approx~~	\sin \theta	~~=~~	\frac{ r }{ 2l }
	\ccb\,,\qquad\qquad\qquad
	\text{so}
	\qquad
	\ccc
	F	~~\simeq~~	\frac{ mg }{ 2l }\,r
\]
\ccb

\vspace*{-0.6cm}
\item[{\color{alizarin}\ding{234}}(\arabic{enumi})]
	What is the magnitude \cc{ F } of the Coulomb force acting on each ball?
	Is the magnitude of the force the same for each ball?
	How do you know?
	If the charge on the two balls were not equal, could you tell?
	Why or why not?
	Is the small \cc{ \theta } approximation valid?

\item[{\color{alizarin}\ding{234}}(\arabic{enumi})]
	Assuming that the equalization procedure in Step~\ref{equalization} worked,
	find the actual charge \cc{ Q } on the balls using the expression for the Coulomb force
	in the introduction

\item
	Noting that the charge on one electron is \cc{ e ~=~ \SI{1.603e-19}{\coulomb} },
	estimate how many electrons were transferred when you charged up the pith balls initially.
	Assuming the pith ball has an electron density similar to that of aluminum
	(\cc{ \SI{8d23}{electron\per\cubic\cm} }),
	estimate the fractional change in the number of electrons which occurred when the balls were charged
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                A N A L Y S I S                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Analysis}}

	Address the questions and observations throughout the laboratory procedures
	in narrative, calculation, graph, sketch or table format as appropriate
	(those questions were highlighted with {\color{alizarin}\ding{234}} sign
	for your convenience)


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   L A B O R A T O R Y   E V A L U A T I O N                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Laboratory evaluation}}

	Please provide feedback on the following areas, comparing this laboratory to your previous labs.
	Please assign each of the listed categories a value in 1 ~--~ 5, with 5 being the best, 1 the worst.

\bigskip
\begin{itemize}
\item
	how much fun you had completing this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well the lab preparation period explained this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the amount of work required compared to the time allotted?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	your understanding of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the difficulty of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well this laboratory tied in with the lecture?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5
\end{itemize}

\bigskip\noindent
	Comments supporting or elaborating on your assessment can also be very helpful in improving the future laboratories


\end{document}
